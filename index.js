const jimp = require('jimp');

exports.handler = async (event) => {
  try {
    const requestBody = JSON.parse(event.body);

    // load jimpImage (url or base64, no ResourceID for now)
    let jimpImage;
    let input_type; 
    if (requestBody.payload.base64 !== null && requestBody.payload.base64 !== undefined && requestBody.payload.base64 !== '') {
        input_type = "base64";
        const image = requestBody.payload.base64;
        jimpImage = await jimp.read(Buffer.from(image, 'base64'));
    } 
    if (requestBody.payload.url !== null && requestBody.payload.url !== undefined && requestBody.payload.url !== '') {
        input_type = "url";
        const image = requestBody.payload.url;
        jimpImage = await jimp.read(image);
    }

    // for each crop requested
    let crops_output = [];
    requestBody.crops.forEach(async (crop_item, index) => {
        image_output = jimpImage;
        // if there is cropping info
        if (crop_item.crop !== null && crop_item.crop !== undefined && crop_item.crop !== ''){
            // if cropping type is BboxExtended
            if ('x' in crop_item.crop && 'y' in crop_item.crop && 'width' in crop_item.crop && 'height' in crop_item.crop){
                image_output.crop(crop_item.crop.x, crop_item.crop.y, crop_item.crop.width, crop_item.crop.height);
            // if cropping type is Bbox
            } else {
                // bbox: crop[x1,y1,x2,y2] --> x,y,width,height
                const width = crop_item.crop[2] - crop_item.crop[0];
                const height = crop_item.crop[3] - crop_item.crop[1];
                image_output.crop(crop_item.crop[0], crop_item.crop[1], width, height);
            }
        }
        // if there is resize info
        if (crop_item.resize !== null && crop_item.resize !== undefined && crop_item.resize !== ''){
            // if resize type is Scale
            if ('scalex' in crop_item.resize && 'scaley' in crop_item.resize){
                const new_width = jimpImage.getWidth() * crop_item.resize.scalex;
                const new_height = jimpImage.getHeight() * crop_item.resize.scaley; 
                image_output.resize(new_width, new_height);

            // if resize type is ScaleExtended
            } else {
                image_output.resize(crop_item.resize.width, crop_item.resize.height);
            }
        }
        // transform crops from JimpImage to base64
        if (input_type == "base64") {
            const outputImageBuffer = await image_output.getBufferAsync(jimp.AUTO);
            base64_image = outputImageBuffer.toString('base64');
        }
        if (input_type == "url"){
            base64_image = await image_output.getBase64Async(jimp.AUTO);
        }

        crops_output[index] = {
            position: index,
            cropped: base64_image
        };
    });

    // build response (for now only returning base64)
    const response = {
      request_id: "asdfghjk",
      crops: crops_output
    };

    return {
      statusCode: 200,
      body: response
    };
  } catch (error) {
    console.error('Error during image cropping/resizing:', error);
    return {
      statusCode: 500,
      body: 'Error during image cropping/resizing'
    };
  }
};
